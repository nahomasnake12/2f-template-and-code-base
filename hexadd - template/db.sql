create table companies (
 id SERIAL primary key,
 company_name varchar,
 created_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
 updated_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
);

create table roles (
id SERIAL primary key,
name varchar,
 created_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
 updated_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP
)

create table sender_ids (
id SERIAL primary key,
title varchar,
company_id integer,
created_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP
)

create table sms_groups(
id SERIAL primary key,
sent_by integer,
name varchar,
company_id int,
created_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP
)

create table sms(
id SERIAL primary key,
phone_number varchar,
body varchar,
message_id varchar,
num_parts varchar,
sms_group_id int,
status varchar,
error_code int,
error_message varchar,
template_id varchar,
sender_id int,
created_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP
)

create table templates (
id SERIAL primary key,
format varchar,
company_id int,
created_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP
)

create table users (
id SERIAL primary key,
name varchar,
username varchar,
role_id int,
company_id int,
created_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP
)

