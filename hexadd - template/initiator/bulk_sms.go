package initiator

import (
	"bulk_sms/platform/pgx"
	router "bulk_sms/platform/routers/httprouter"

	"fmt"
	"os"
)

func GetDbString() pgx.Pgx {
	dbUser := os.Getenv("DB_USER")
	dbName := os.Getenv("DB_NAME")
	dbPass := os.Getenv("DB_PASSWORD")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")

	dsn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s", dbUser, dbPass, dbHost, dbPort, dbName)
	fmt.Println(dsn)
	return pgx.InitDataBase(dsn)

}

func ServeRest(host, port string, domainRoutes ...[]router.Router) {
	var routes []router.Router = nil
	for _, route := range domainRoutes {
		routes = append(routes, route...)
	}

	// routes = routers.Initialize(host, port, routes)
	fmt.Println(routes)
}

func Initiate() {
	// initial all domains
	smsRoutes := Sms()
	userRoutes := User()

	host := os.Getenv("REST_HOST")
	port := os.Getenv("REST_PORT")
	ServeRest(host, port, smsRoutes, userRoutes)

	// serverHost := os.Getenv("SERVER_HOST")

}
