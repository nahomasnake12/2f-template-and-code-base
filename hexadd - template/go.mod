module bulk_sms

go 1.16

require (
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible // indirect
	github.com/jackc/pgx/v4 v4.11.0
	github.com/julienschmidt/httprouter v1.2.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/rileyr/middleware v0.0.0-20200807231548-33fa83c97727
	github.com/stretchr/testify v1.6.1 // indirect
)
