package pgx

import (
	"context"

	"github.com/jackc/pgx/v4"
)

type connection struct {
	dsn string
}

type Pgx interface {
	GetConnection() (*pgx.Conn, error)
}

func InitDataBase(dsn string) Pgx {
	return &connection{
		dsn,
	}
}

func (con *connection) GetConnection() (*pgx.Conn, error) {
	return pgx.Connect(context.Background(), con.dsn)
}
