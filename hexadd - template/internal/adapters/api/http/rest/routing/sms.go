package routing

import (
	"net/http"

	routers "bulk_sms/platform/routers/httprouter"

	"bulk_sms/internal/adapters/api/http/rest/server/handlers"
)

func OtpRouting(handler handlers.Sms) []routers.Router {
	return []routers.Router{
		{
			Method:      http.MethodGet,
			Path:        "/sms/:id",
			Handler:     handler.UploadSmsFile,
			MiddleWares: nil,
		},
	}
}
