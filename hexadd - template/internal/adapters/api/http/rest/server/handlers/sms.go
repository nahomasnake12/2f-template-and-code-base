package handlers

import (
	smsModule "bulk_sms/internal/module/sms"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type Sms interface {
	UploadSmsFile(w http.ResponseWriter, r *http.Request, _param httprouter.Params)
}

type sms struct {
	useCase smsModule.Usecase
}

func SmsInit(userCase smsModule.Usecase) Sms {
	return &sms{
		userCase,
	}
}

func (sms *sms) UploadSmsFile(w http.ResponseWriter, r *http.Request, _param httprouter.Params) {

}
