package prpc

import (
	"log"
	"net"

	grpcSrv "google.golang.org/grpc"
)

type grpc struct {
	port, host string
}
type Grpc interface {
	Serve() error
}

func Init(port, host string) Grpc {
	return &grpc{
		port,
		host,
	}
}

func (gr *grpc) Serve() error {
	lis, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpcSrv.NewServer()

	if err := grpcServer.Serve(lis); err != nil {
		return err
	}

	return nil
}
