package file

import (
	"net/http"
)

type files struct {
	root string
}

type File interface {
	UploadFileFromRequest(r *http.Request, dstPath string, key string) (string, error)
	GetFileListsFromDir(root string) ([]string, error)
	CleanUp(path string) error
	Root() string
}

func InitFile(root string) File {
	return &files{
		root,
	}
}

// func InitFileByPath(fileDir string) (File, error) {
// 	file, err := os.Open(fileDir)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return &files{
// 		file,
// 	}, nil
// }
