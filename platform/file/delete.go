package file

import "os"

func (fls *files) CleanUp(path string) error {
	err := os.RemoveAll(path)
	return err
}
