package file

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"path"
	"strings"
)

func (fls *files) UploadFileFromRequest(r *http.Request, dstPath string, key string) (string, error) {
	r.ParseMultipartForm(10 << 20)
	multipartFile, handler, err := r.FormFile(key)

	if err != nil || multipartFile == nil || handler == nil {
		return "", fmt.Errorf("%s doesn't contain media file", key)
	}
	dstPath = path.Join(fls.root, dstPath)
	defer multipartFile.Close()

	file, err := ioutil.TempFile(dstPath, "upload-*."+strings.Split(handler.Filename, ".")[1])
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	defer file.Close()

	fileBytes, err := ioutil.ReadAll(multipartFile)
	if err != nil {
		fmt.Println(err)
	}
	file.Write(fileBytes)
	return file.Name(), err
}
