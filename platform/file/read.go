package file

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func (fls *files) GetFileListsFromDir(dir string) ([]string, error) {
	// dir = path.Join(fls.root, dir)
	var files []string
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if path == dir {
			return nil
		}
		files = append(files, path)
		return nil
	})
	if err != nil {
		return nil, err
	}
	return files, nil
}

func (fls *files) GetFile(path string) (content []byte) {
	filepath := fmt.Sprintf("%s", path)
	content, err := ioutil.ReadFile(filepath)
	if err != nil {
		log.Fatal(err.Error())
	}
	return
}
func (fls *files) Root() string {
	return fls.root
}
