package routers

import (
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/rileyr/middleware"
	"github.com/rileyr/middleware/wares"
)

type Routers interface {
	Serve()
}

type Router struct {
	Method      string
	Path        string
	Handler     httprouter.Handle
	MiddleWares []func(handle httprouter.Handle) httprouter.Handle
}
type routing struct {
	host    string
	port    string
	routers []Router
}

// Initialize initialize routing and host
func Initialize(host, port string, routers []Router) Routers {
	return &routing{
		host,
		port,
		routers,
	}
}

func (r *routing) Serve() {
	httpRouter := httprouter.New()
	for _, router := range r.routers {
		if router.MiddleWares == nil {
			httpRouter.Handle(router.Method, router.Path, router.Handler)
		} else {
			s := middleware.NewStack()
			for _, middle := range router.MiddleWares {
				s.Use(middle)
			}
			s.Use(wares.RequestID)
			s.Use(wares.Logging)
			httpRouter.Handle(router.Method, router.Path, s.Wrap(router.Handler))
		}
	}
	addr := fmt.Sprintf("%s", r.host)
	fmt.Println("Testing: ", addr)
	err := http.ListenAndServe(addr, httpRouter)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Serving at %s", r.port)
}
