package ffmpeg

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"strings"
	"unicode/utf8"
)

func removeSlashPrefix(path string) string {
	_, i := utf8.DecodeRuneInString(path)
	return path[i:]
}

// ConvertToHlsAudio ... (audiosrcfolder string, audiosrc string, destinationpath string)
// accepts a raw audio file as src and provides the adts equivalent in the destination path
func (ffmpeg *ffmpeg) ConvertToHlsAudio(audiosrc string, destinationpath string) (Stream, error) {
	dstPath := path.Join(ffmpeg.rootDIR, destinationpath)
	srcPath := audiosrc
	err := os.MkdirAll(dstPath, 0700)
	var stream Stream

	if err != nil {
		return stream, err
	}

	if len(strings.Split(dstPath, ffmpeg.rootDIR)) < 2 {
		return stream, errors.New("ConvertToHlsAudio: Invalid Destination path")
	}

	audioRelativePath := strings.Split(dstPath, ffmpeg.rootDIR)[1]

	stream.AudioIndexManifestPath = removeSlashPrefix(audioRelativePath + "/" + "main.m3u8")
	stream.AudioStreamLocation = removeSlashPrefix(audioRelativePath)
	stream.RawAudioLocation = audiosrc

	secret, err := exec.Command("openssl", "rand", "16").Output()
	secretLocation := path.Join(dstPath, "enc.key")
	err = ioutil.WriteFile(secretLocation, secret, 0644)
	// if err

	iv, err := exec.Command("openssl", "rand", "-hex", "16").Output()
	fmt.Println(iv, err)

	kefInfo := fmt.Sprintf("%s\n%s\n%s", secretLocation, secretLocation, iv)
	keyInfoFile := path.Join(dstPath, "keyInfo.txt")
	err = ioutil.WriteFile(keyInfoFile, []byte(kefInfo), 0644)

	fmt.Println(keyInfoFile)
	if err != nil {
		return stream, err
	}
	defer os.Remove(keyInfoFile)
	fmt.Println("ffmpeg", "-i", srcPath, "-hls_key_info_file", keyInfoFile, "-c:v", "libx264", "-c:a", "aac", "-strict", "-2", "-f", "hls", "-hls_list_size", "0", path.Join(dstPath, "main.m3u8"))
	_, err = exec.Command("ffmpeg", "-i", srcPath, "-hls_key_info_file", keyInfoFile, "-c:v", "libx264", "-c:a", "aac", "-strict", "-2", "-f", "hls", "-hls_list_size", "0", path.Join(dstPath, "main.m3u8")).Output()

	fmt.Println(err)
	if err != nil {
		return stream, errors.New("error accessing source file ")
	}
	return stream, err

}
func (ffmpeg *ffmpeg) Root() string {
	return ffmpeg.rootDIR
}
