package ffmpeg

import "path"

type ffmpeg struct {
	rootDIR string
}

type Stream struct {
	AudioStreamLocation    string
	RawAudioLocation       string
	AudioIndexManifestPath string
}

const (
	audioFolder = "tracks"
)

type FFmpeg interface {
	ConvertToHlsAudio(audiosrcname string, destinationpath string) (Stream, error)
	Root() string
}

func NewFFmpeg(root string) FFmpeg {
	return &ffmpeg{rootDIR: path.Join(root, audioFolder)}
}

func (ffmpeg *ffmpeg) Encode(fth string, dst string) error {

	return nil
}
