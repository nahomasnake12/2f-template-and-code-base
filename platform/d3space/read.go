package d3space

import (
	"bytes"
	"fmt"
	"io"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
)

func (spc *space) ListFilesFromServer(bucket string) (s3.Object, error) {
	input := &s3.ListObjectsInput{
		Bucket: aws.String(bucket),
	}

	objects, err := s3.New(spc.session).ListObjects(input)
	if err != nil {
		return s3.Object{}, err
	}

	for _, obj := range objects.Contents {
		fmt.Println(aws.StringValue(obj.Key))
	}
	return s3.Object{}, nil
}

func (spc *space) GetAllInaFolder(subfolder string) ([]string, error) {
	var objectKeys []string

	input := &s3.ListObjectsInput{
		Bucket: aws.String(spc.bucket),
		Prefix: &subfolder,
	}

	objects, err := s3.New(spc.session).ListObjects(input)
	if err != nil {
		return nil, err
	}

	for _, obj := range objects.Contents {
		objectKeys = append(objectKeys, (aws.StringValue(obj.Key)))
	}
	return objectKeys, nil
}

func (spc *space) GetUrlForObject(object string, durMin int) (string, error) {
	s3Client := s3.New(spc.session)
	req, _ := s3Client.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(spc.bucket),
		Key:    aws.String(object),
	})

	urlStr, err := req.Presign(time.Duration(durMin) * time.Minute)
	return urlStr, err
}

func (spc *space) GetFileFromD3(object string) (io.ReadCloser, error) {

	s3Client := s3.New(spc.session)

	input := &s3.GetObjectInput{
		Bucket: aws.String(spc.bucket),
		Key:    aws.String(object),
	}

	result, err := s3Client.GetObject(input)

	return result.Body, err
}

// directly return text data given the object key.
func (spc *space) GetTextDataFromD3(object string) (string, error) {
	file, err := spc.GetFileFromD3(object)
	if err != nil {
		return "", err
	}
	buf := new(bytes.Buffer)
	buf.ReadFrom(file)
	newStr := buf.String()
	return newStr, nil
}
