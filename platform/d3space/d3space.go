package d3space

import (
	"fmt"
	"io"
	"os"

	ctrackgrpc "otp_service/internal/adapters/grpc/client"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type space struct {
	session    *session.Session
	region     string
	endpoint   string
	key        string
	bucket     string
	grpcClient *ctrackgrpc.TrackClient
}

type Space interface {
	GetFileFromD3(object string) (io.ReadCloser, error)
	PushFile(string, *os.File) error
	PushMultipleFiles([]string) error
	DeleteObject() (s3.DeleteObjectOutput, error)
	GetAllInaFolder(subfolder string) ([]string, error)
	GetUrlForObject(object string, duration int) (string, error)
	GetTextDataFromD3(object string) (string, error)
}

// InitSpace ... initialize space with configs .
func InitSpace(secret string, key string, bucket string, endpoint string, region string, grpcClient *ctrackgrpc.TrackClient) (Space, error) {

	if secret == "" || key == "" || endpoint == "" || region == "" {
		return nil, fmt.Errorf("D3Space Init, Invalid params")
	}

	s3Config := &aws.Config{
		Credentials: credentials.NewStaticCredentials(key, secret, ""),
		Endpoint:    aws.String(endpoint),
		Region:      aws.String(region),
	}

	session, err := session.NewSession(s3Config)
	if err != nil {
		return nil, err
	}

	return &space{
		session,
		region,
		endpoint,
		key,
		bucket,
		grpcClient,
	}, nil
}

// upload to space
// upload mulitple files to space
//
