package d3space

import (
	"bytes"
	"fmt"
	"net/http"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
)

// PushFile ... Push a single file from local to server.
func (spc *space) PushFile(path string, file *os.File) error {
	fileInfo, _ := file.Stat()
	size := fileInfo.Size()
	bufferData := make([]byte, size)
	_, err := s3.New(spc.session).PutObject(&s3.PutObjectInput{
		Bucket:             aws.String(spc.bucket),
		Key:                aws.String(path),
		Body:               bytes.NewReader(bufferData),
		ContentDisposition: aws.String("attachment"),
		ContentLength:      aws.Int64(size),
		ContentType:        aws.String(http.DetectContentType(bufferData)),
	})
	return err
}

// PushMultipleFiles push multiple files to d3.
func (spc *space) PushMultipleFiles(paths []string) error {
	for _, path := range paths {
		file, err := os.Open(path)
		if err != nil {
			return err
		}
		defer file.Close()

		if err != nil {
			fmt.Println(err)
		}
		err = spc.PushFile(path, file)
		if err != nil {
			fmt.Println(err)
		}
		if err != nil {
			return err
		}
	}
	return nil
}
