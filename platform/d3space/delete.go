package d3space

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
)

func (spc *space) DeleteObject() (s3.DeleteObjectOutput, error) {
	remoteFile := &s3.DeleteObjectInput{
		Bucket: aws.String(spc.bucket),
		Key:    aws.String(spc.key),
	}
	s3Client := s3.New(spc.session)
	result, err := s3Client.DeleteObject(remoteFile)
	if err != nil {
		return s3.DeleteObjectOutput{}, err
	}
	return *result, nil
}
