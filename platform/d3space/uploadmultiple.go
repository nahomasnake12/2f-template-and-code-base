package d3space

// import "os"

// var (
// 	MaxWorker = os.Getenv("MAX_WORKERS")
// 	MaxQueue  = os.Getenv("MAX_QUEUE")
// )

// type Job struct {
// 	Payload Payload
// }

// var JobQueue chan Job

// type Worker struct {
// 	WorkerPool chan chan Job
// 	JobChannel chan Job
// 	quit       chan bool
// }

// func NewWorker(workerPool chan chan Job) Worker {
// 	return Worker{
// 		WorkerPool: workerPool,
// 		JobChannel: make(chan Job),
// 		quit:       make(chan bool)}
// }

// func (w Worker) Start() {
// 	go func() {
// 		for {
// 			w.WorkerPool <- w.JobChannel

// 			select {
// 			case job := <-w.JobChannel:
// 				if err := job.Payload.UploadToS3(); err != nil {
// 					log.Errorf("Error uploading to S3: %s", err.Error())
// 				}

// 			case <-w.quit:
// 				return
// 			}
// 		}
// 	}()
// }

// type Dispatcher struct {
// 	WorkerPool chan chan Job
// }

// func NewDispatcher(maxWorkers int) *Dispatcher {
// 	pool := make(chan chan Job, maxWorkers)
// 	return &Dispatcher{WorkerPool: pool}
// }

// func (d *Dispatcher) Run() {
// 	for i := 0; i < d.maxWorkers; i++ {
// 		worker := NewWorker(d.pool)
// 		worker.Start()
// 	}

// 	go d.dispatch()
// }

// func (d *Dispatcher) dispatch() {
// 	for {
// 		select {
// 		case job := <-JobQueue:
// 			go func(job Job) {
// 				jobChannel := <-d.WorkerPool

// 				jobChannel <- job
// 			}(job)
// 		}
// 	}
// }

// func (w Worker) Stop() {
// 	go func() {
// 		w.quit <- true
// 	}()
// }
