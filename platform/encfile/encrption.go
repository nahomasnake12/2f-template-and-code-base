package encfile

import (
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"time"

	"github.com/purnaresa/bulwark/encryption"
	"github.com/purnaresa/bulwark/utils"
)

func (efile *aes) Encrypt(secret string, filePath string) error {
	file := utils.ReadFile(filePath)
	err := os.MkdirAll(filepath.Dir(path.Join(efile.root, filePath)), 0700)

	fmt.Println(filePath, filepath.Dir(filePath))
	if err != nil {
		return err
	}

	encryptionClient := encryption.NewClient()
	cipherFile := encryptionClient.EncryptAES(file, []byte(secret))

	err = utils.WriteFile(cipherFile, path.Join(efile.root, filePath))
	if err != nil {
		log.Fatalln(err)
	}
	// encryption end

	return nil
}

func (efile *aes) EncryptMultiple(filePaths []string) (string, error) {
	// generate proper key
	encryptionClient := encryption.NewClient()
	secret := encryptionClient.GenerateRandomString(32)

	fmt.Println("FilePaths", filePaths)

	if len(filePaths) < 1 {
		return "", fmt.Errorf("error : couldn't not read files to transcode ")
	}
	for _, filePath := range filePaths {
		fmt.Println("file ----", filePath)
		err := efile.Encrypt(secret, filePath)

		if err != nil {
			return "", err
		}
	}
	keyName :=
		fmt.Sprintf("%-key.txt", strconv.Itoa(int(time.Now().Unix())))
	err := utils.WriteFile([]byte(secret), path.Join(efile.keyLocation, keyName))
	if err != nil {
		return "", err
	}

	return keyName, nil
}

func (efile *aes) Decrypt() error {
	return nil
}

func (efile *aes) Root() string {
	return efile.root
}
