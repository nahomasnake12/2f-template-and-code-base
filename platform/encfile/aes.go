package encfile

type aes struct {
	keyLocation string
	root        string
}

type Aes interface {
	Encrypt(secret string, filePath string) error
	Decrypt() error
	EncryptMultiple(filePaths []string) (string, error)
	Root() string
}

func NewAes(root string, keyLocation string) Aes {
	return &aes{
		keyLocation: keyLocation,
		root:        root,
	}
}
