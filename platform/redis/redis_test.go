package redis

import (
	"reflect"
	"testing"
)

func TestInitialize(t *testing.T) {
	type args struct {
		connection string
		password   string
		domain     string
	}
	tests := []struct {
		name string
		args args
		want Connections
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Initialize(tt.args.connection, tt.args.password, tt.args.domain); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Initialize() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_connectionString_Open(t *testing.T) {
	type fields struct {
		connection string
		password   string
		domain     string
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			"basic",
			fields{
				"localhost:6379",
				"",
				"",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cs := &connectionString{
				connection: tt.fields.connection,
				password:   tt.fields.password,
				domain:     tt.fields.domain,
			}
			if _, err := cs.Open(); err != nil {
				t.Errorf("connectionString.Open() = %v", err)
			}
		})
	}
}
