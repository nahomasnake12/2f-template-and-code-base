package m3u8

type m3u8 struct {
}

type M3u8 interface {
	ParseDuration() (uint, error)
	AddEncrption(string) error
}

func InitM3u8() M3u8 {
	return &m3u8{}
}
